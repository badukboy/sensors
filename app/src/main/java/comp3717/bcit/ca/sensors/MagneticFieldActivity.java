package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class MagneticFieldActivity
    extends SensorActivity
{
    private static final String TAG = MagneticFieldActivity.class.getName();

    public MagneticFieldActivity()
    {
        super(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("X-bias: " + Float.toString(event.values[0]));
        values.add("Y-bias: " + Float.toString(event.values[1]));
        values.add("Z-bias: " + Float.toString(event.values[2]));

        return (values);
    }
}
