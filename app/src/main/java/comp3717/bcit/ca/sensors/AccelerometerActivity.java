package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class AccelerometerActivity
    extends SensorActivity
{
    private static final String TAG = AccelerometerActivity.class.getName();

    public AccelerometerActivity()
    {
        super(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("X-axis: " + Float.toString(event.values[0]));
        values.add("Y-axis: " + Float.toString(event.values[1]));
        values.add("Z-axis: " + Float.toString(event.values[2]));

        return (values);
    }
}
