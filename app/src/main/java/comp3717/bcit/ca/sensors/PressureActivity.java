package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class PressureActivity
    extends SensorActivity
{
    private static final String TAG = PressureActivity.class.getName();

    public PressureActivity()
    {
        super(Sensor.TYPE_PRESSURE);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("hPa: " + Float.toString(event.values[0]));

        return (values);
    }
}
