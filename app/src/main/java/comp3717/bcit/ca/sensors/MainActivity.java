package comp3717.bcit.ca.sensors;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MainActivity
    extends ListActivity
{
    private final Map<String, Class<? extends Activity>> values;

    {
        final Map<String, Class<? extends Activity>> tempValues;

        tempValues = new LinkedHashMap<>();
        tempValues.put("Accelerometer",               AccelerometerActivity.class);
        tempValues.put("Gravity",                     GravityActivity.class);
        tempValues.put("Gyroscope",                   GyroscopeActivity.class);
        tempValues.put("Gyroscope Uncalibrated",      GyroscopeUncalibratedActivity.class);
        tempValues.put("Linear Acceleration",         LinearAccelerationActivity.class);
        tempValues.put("Rotation Vector",             RotationVectorActivity.class);
        tempValues.put("Significant Motion",          SignificantMotionActivity.class);
        tempValues.put("Step Counter",                StepCounterActivity.class);
        tempValues.put("Step Detector",               StepDetectorActivity.class);
        tempValues.put("Game Rotation Vector",        GameRotationVectorActivity.class);
        tempValues.put("Geomagnetic Rotation Vector", GeomagneticRotationVectorActivity.class);
        tempValues.put("Magnetic Field",              MagneticFieldActivity.class);
        tempValues.put("Magnetic Field Uncalibrated", MagneticFieldUncalibratedActivity.class);
        tempValues.put("Proximity",                   ProximityActivity.class);
        tempValues.put("Ambient Temperature",         AmbientTemperatureActivity.class);
        tempValues.put("Light",                       LightActivity.class);
        tempValues.put("Pressure",                    PressureActivity.class);
        tempValues.put("Relative Humidity",           RelativeHumidityActivity.class);
        values = Collections.unmodifiableMap(tempValues);
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final Set<String>  keysSet;
        final List<String> keysArray;
        final ListAdapter  adapter;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        keysSet   = values.keySet();
        keysArray = new ArrayList<>(keysSet);
        adapter   = new ArrayAdapter<String>(this,
                                             android.R.layout.simple_list_item_1,
                                             keysArray);
        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(final ListView listView,
                                   final View     view,
                                   final int      position,
                                   final long     id)
    {
        final String                    item;
        final Class<? extends Activity> clazz;
        final Intent                    intent;

        item   = (String)getListAdapter().getItem(position);
        clazz  = values.get(item);
        intent = new Intent(this, clazz);
        startActivity(intent);
    }
}
