package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class AmbientTemperatureActivity
    extends SensorActivity
{
    private static final String TAG = AmbientTemperatureActivity.class.getName();

    public AmbientTemperatureActivity()
    {
        super(Sensor.TYPE_AMBIENT_TEMPERATURE);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("Degrees C: " + Float.toString(event.values[0]));

        return (values);
    }
}
