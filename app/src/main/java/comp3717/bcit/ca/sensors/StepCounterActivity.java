package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class StepCounterActivity
    extends SensorActivity
{
    private static final String TAG = StepCounterActivity.class.getName();

    public StepCounterActivity()
    {
        super(Sensor.TYPE_STEP_COUNTER);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("Steps: " + Float.toString(event.values[0]));

        return (values);
    }
}
