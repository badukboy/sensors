package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class MagneticFieldUncalibratedActivity
    extends SensorActivity
{
    private static final String TAG = MagneticFieldUncalibratedActivity.class.getName();

    public MagneticFieldUncalibratedActivity()
    {
        super(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("X-bias w/o iron: " + Float.toString(event.values[0]));
        values.add("Y-bias w/o iron: " + Float.toString(event.values[1]));
        values.add("Z-bias w/o iron: " + Float.toString(event.values[2]));
        values.add("X-bias: " + Float.toString(event.values[3]));
        values.add("Y-bias: " + Float.toString(event.values[4]));
        values.add("Z-bias: " + Float.toString(event.values[5]));

        return (values);
    }}
