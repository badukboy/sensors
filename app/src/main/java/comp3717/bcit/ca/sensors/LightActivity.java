package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class LightActivity
    extends SensorActivity
{
    private static final String TAG = LightActivity.class.getName();

    public LightActivity()
    {
        super(Sensor.TYPE_LIGHT);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("Lux: " + Float.toString(event.values[0]));

        return (values);
    }
}
