package comp3717.bcit.ca.sensors;

import android.app.ListActivity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public abstract class SensorActivity
    extends ListActivity
    implements SensorEventListener
{
    private final int           type;
    private       SensorManager sensorManager;
    private       Sensor        sensor;
    private ArrayAdapter        adapter;


    protected SensorActivity(final int t)
    {
        type = t;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        final List<String> values;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        setupSensor();
        values = new ArrayList<>();

        if(hasSensor())
        {
            values.add("Present");
        }
        else
        {
            values.add("Not present");
        }

        adapter = new ArrayAdapter<String>(this,
                                           android.R.layout.simple_list_item_1,
                                           values);
        setListAdapter(adapter);
    }

    protected final void setupSensor()
    {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensor        = sensorManager.getDefaultSensor(type);
    }

    protected final boolean hasSensor()
    {
        return (sensor != null);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this,
                                       sensor,
                                       SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected final void onListItemClick(final ListView listView,
                                         final View     view,
                                         final int      position,
                                         final long     id)
    {
    }

    @Override
    public final void onAccuracyChanged(final Sensor sensor,
                                        final int    accuracy)
    {
    }

    @Override
    public void onSensorChanged(final SensorEvent event)
    {
        final List<String> values;

        adapter.clear();
        adapter.add(sensor.getName());
        adapter.add(Integer.toString(sensor.getType()));
        adapter.add(sensor.getVendor());
        adapter.add(event.timestamp);
        values = getValues(event);
        adapter.addAll(values);

        runOnUiThread(new Runnable()
        {
            public void run()
            {
                Log.d(TAG, "notifyDataSetChanged");
                adapter.notifyDataSetChanged();
            }
        });
    }

    protected abstract List<String> getValues(SensorEvent event);
}
