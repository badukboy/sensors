package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.Collections;
import java.util.List;

public class SignificantMotionActivity
    extends SensorActivity
{
    private static final String TAG = SignificantMotionActivity.class.getName();

    public SignificantMotionActivity()
    {
        super(Sensor.TYPE_SIGNIFICANT_MOTION);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = Collections.EMPTY_LIST;

        return (values);
    }
}
