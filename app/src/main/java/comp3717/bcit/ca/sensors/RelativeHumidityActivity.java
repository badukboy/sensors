package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class RelativeHumidityActivity
    extends SensorActivity
{
    private static final String TAG = RelativeHumidityActivity.class.getName();

    public RelativeHumidityActivity()
    {
        super(Sensor.TYPE_RELATIVE_HUMIDITY);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("Percent: " + Float.toString(event.values[0]));

        return (values);
    }
}
