package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class GeomagneticRotationVectorActivity
    extends SensorActivity
{
    private static final String TAG = GeomagneticRotationVectorActivity.class.getName();

    public GeomagneticRotationVectorActivity()
    {
        super(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("X-axis: " + Float.toString(event.values[0]));
        values.add("Y-axis: " + Float.toString(event.values[1]));
        values.add("Z-axis: " + Float.toString(event.values[2]));

        return (values);
    }
}
