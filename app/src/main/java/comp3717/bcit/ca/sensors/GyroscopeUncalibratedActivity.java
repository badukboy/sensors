package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class GyroscopeUncalibratedActivity
    extends SensorActivity
{
    private static final String TAG = GyroscopeUncalibratedActivity.class.getName();

    public GyroscopeUncalibratedActivity()
    {
        super(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("X-axis w/o drift: " + Float.toString(event.values[0]));
        values.add("Y-axis w/o drift: " + Float.toString(event.values[1]));
        values.add("Z-axis w/o drift: " + Float.toString(event.values[2]));

        // some older devices do not include these...
        if(event.values.length > 3)
        {
            values.add("X-axis: " + Float.toString(event.values[3]));
            values.add("Y-axis: " + Float.toString(event.values[4]));
            values.add("Z-axis: " + Float.toString(event.values[5]));
        }

        return (values);
    }
}
