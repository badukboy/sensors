package comp3717.bcit.ca.sensors;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import java.util.ArrayList;
import java.util.List;

public class ProximityActivity
    extends SensorActivity
{
    private static final String TAG = ProximityActivity.class.getName();

    public ProximityActivity()
    {
        super(Sensor.TYPE_PROXIMITY);
    }

    @Override
    protected List<String> getValues(final SensorEvent event)
    {
        final List<String> values;

        values = new ArrayList<>();
        values.add("cm: " + Float.toString(event.values[0]));

        return (values);
    }
}
